import 'package:flutter/material.dart';

class CryptoCoinScreen extends StatefulWidget {
  const CryptoCoinScreen({super.key});

  @override
  State<StatefulWidget> createState() => _CryptoCoinScreenState();
}

class _CryptoCoinScreenState extends State {
  String? coinName;

  @override
  void didChangeDependencies() {
    final args = ModalRoute.of(context)?.settings.arguments;
    assert(args != null && args is String, 'You must provide string args');
    // if (args == null) {
    //   print('provide aguments');
    //   return;
    // }
    // if (args is! String) {
    //   print('you must provide string');
    //   return;
    // }
    coinName = args as String;
    setState(() {});
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          coinName ?? '...',
        ),
      ),
    );
  }
}
