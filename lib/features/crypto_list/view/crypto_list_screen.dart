import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CryptoListScreen extends StatefulWidget {
  const CryptoListScreen({super.key});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  State<CryptoListScreen> createState() => _CryptoListScreenState();
}

class _CryptoListScreenState extends State<CryptoListScreen> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Crypto Horse',
          ),
        ),
        body: ListView.separated(
            itemCount: 10,
            separatorBuilder: (context, i) => const Divider(),
            itemBuilder: (context, i) {
              const coinName = 'Bitcoint';

              return ListTile(
                  leading: SvgPicture.asset(
                    'assets/svg/bitcoin-logo.svg',
                    width: 22,
                    height: 22,
                  ),
                  title: Text(
                    coinName,
                    style: theme.textTheme.bodyMedium,
                  ),
                  subtitle: Text(
                    '\$20',
                    style: theme.textTheme.bodySmall,
                  ),
                  trailing: const Icon(Icons.arrow_forward),
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed('/coin', arguments: coinName);
                  });
            }));
  }
}
