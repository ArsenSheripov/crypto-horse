import 'package:flutter/material.dart';

final darkTheme = ThemeData(
    useMaterial3: true,
    colorScheme: ColorScheme.fromSeed(seedColor: Colors.yellow),
    scaffoldBackgroundColor: const Color.fromARGB(255, 31, 31, 31),
    appBarTheme: const AppBarTheme(
      backgroundColor: Color.fromARGB(255, 241, 253, 6),
    ),
    textTheme: const TextTheme(
        bodyMedium: TextStyle(
          fontSize: 20,
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        bodySmall: TextStyle(
          fontSize: 14,
          color: Color.fromARGB(255, 255, 255, 255),
        )),
    dividerColor: const Color.fromARGB(146, 210, 208, 208),
    listTileTheme:
        const ListTileThemeData(iconColor: Color.fromARGB(146, 210, 208, 208)));
